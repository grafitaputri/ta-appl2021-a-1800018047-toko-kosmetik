<?php
    include('conn.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Temu 8 - 11</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
<!-- Demo header-->
<section class="header text-center">
    <div class="container text-white">
        <header class="py-5">
            <h1 class="display-4">Kosmetiku Store</h1>
            <p class="font-italic mb-1">Beli kosmetik murah, berkualitas, dan terpercaya</p>
        </header>
    </div>
</section>


<!-- Sticky navbar-->
<header class="header sticky-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">
        <div class="container"><a class="navbar-brand" href="#">
            <strong class="h6 mb-0 font-weight-bold text-uppercase">Kosmetiku</strong></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a class="nav-link" href="#">Product </a></li>
                    <li class="nav-item active"><a class="nav-link" href="tambah_produk.php">Tambah Produk</a></li>
                    <li class="nav-item active"><a class="nav-link" href="#">Bantuan</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- Demo content-->
<section class="py-5 section-1">
    <div class="row">
        <div class="col-lg-3">
            <div class="card rounded shadow-sm border-0 ml-3">
                <div class="card-body">
                <h5>Tambah Produk</h5>
                <form action="proses_tambah.php" method="POST">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input type="text" onkeypress="return huruf(event)" class="form-control" id="nama" name="nama">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Deskripsi</label>
                      <textarea class="form-control" onkeypress="return huruf(event)" name="deskripsi" id="deskripsi"></textarea>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Harga</label>
                      <input type="text" class="form-control" onkeypress="return angka(event)" id="harga" name="harga">
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>`
                </div>
            </div>
        </div>
        <div class="col-lg-9">
        <div class="row">
          <div class="col-5">
          <form action="index.php" method="get"> 
            <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4">
                <div class="input-group">
                  <input type="text" name="cari" placeholder="What're you searching for?" aria-describedby="button-addon1" class="form-control border-0 bg-light">
                  <div class="input-group-append">
                    <button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
            </div>
        </form>
        <?php 
            if(isset($_GET['cari'])){
              $cari = $_GET['cari'];
            }
        ?>
          </div>
        </div>
        <div class="row">
        <?php 
            if(isset($_GET['cari'])){
              $cari = $_GET['cari'];
              $data = mysqli_query($koneksi, "SELECT * FROM produk where nama_produk like '%".$cari."%'");       
            }else{
              $data = mysqli_query($koneksi, "SELECT * FROM produk");   
            }
            while($d = mysqli_fetch_array($data)){
            ?>
          <div class="col-xl-3 col-lg-4 col-md-6 mb-4 mr-3">
            <div class="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294929/matthew-hamilton-351641-unsplash_zmvozs.jpg" alt="" class="img-fluid card-img-top">
              <div class="p-4">
                <h5> <a href="#" class="text-dark"><?=$d['nama_produk']?></a></h5>
                <p class="small text-muted mb-0"><?=$d['deskripsi']?></p>
                <div class="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
                  <p class="small mb-0"><?=$d['harga']?></p>
                  <div class="badge badge-danger px-3 rounded-pill font-weight-normal">New</div>
                </div>
              </div>
            </div>
          </div>      
            <?php } ?>    
        </div>
    </div>
</section>

 <!-- Footer -->
  <footer class="bg-white">
    <div class="container py-5">
      <div class="row py-4">
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0"><img src="img/logo.png" alt="" width="180" class="mb-3">
          <p class="font-italic text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
          <ul class="list-inline mt-4">
            <li class="list-inline-item"><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="vimeo"><i class="fa fa-vimeo"></i></a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Shop</h6>
          <ul class="list-unstyled mb-0">
            <li class="mb-2"><a href="#" class="text-muted">For Women</a></li>
            <li class="mb-2"><a href="#" class="text-muted">For Men</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Stores</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Our Blog</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Company</h6>
          <ul class="list-unstyled mb-0">
            <li class="mb-2"><a href="#" class="text-muted">Login</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Register</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Wishlist</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Our Products</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Newsletter</h6>
          <p class="text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At itaque temporibus.</p>
          <div class="p-1 rounded border">
            <div class="input-group">
              <input type="email" placeholder="Enter your email address" aria-describedby="button-addon1" class="form-control border-0 shadow-0">
              <div class="input-group-append">
                <button id="button-addon1" type="submit" class="btn btn-link"><i class="fa fa-paper-plane"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Copyrights -->
    <div class="bg-light py-4">
      <div class="container text-center">
        <p class="text-muted mb-0 py-2">© 2019 Bootstrapious All rights reserved.</p>
      </div>
    </div>
  </footer>
  <!-- End -->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script type="text/javascript">
    function angka(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 48 || charCode > 57)&&charCode>32){
            alert("Masukkan umur anda");
            return false;
        }
        return true;
    }
        
    function huruf(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32) {
            alert("Masukkan hanya berupa huruf");
            return false;
        }
        return true;
    }
</script>
</body>
</html>